#!/bin/bash
#SBATCH --job-name=ProtonTrapSimulation
#SBATCH --mail-type=END,FAIL,REQUEUE
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=2gb
#SBATCH --time=01:00:00
#SBATCH --output=%A_%4a.out
#SBATCH --requeue
#SBATCH --array=1-1

#
# Usage:
#   sbatch --partition=compute --array=1-100 --time=24:00:00 job.sh events=100
#

SUBMITDIR=`pwd`

JOB=${SLURM_ARRAY_JOB_ID:-0}
TASK=${SLURM_ARRAY_TASK_ID:-0}
TASK=`printf "%04d" ${TASK}`

# Find container
SIF=$HOME/git/bl3-kassiopeia/kassiopeia_latest.sif
if [ ! -f ${SIF} -a ! -x ${SIF} ] ; then
  echo "Could not find or execute ${SIF}"
  echo "Run 'singularity pull docker://katrinexperiment/kassiopeia:latest' first"
  exit
fi

# Job configuration
JOBDIR=$HOME/scratch/${JOB}
mkdir -p ${JOBDIR}
JOBDIR=`readlink -f ${JOBDIR}`
cd ${JOBDIR}

echo "Current working directory is `pwd`"

# Task configuration
TASKDIR=${JOBDIR}/${TASK}
mkdir -p $TASKDIR
TASKDIR=`readlink -f ${TASKDIR}`
cd ${TASKDIR}

echo "Current working directory is `pwd`"

# Determine bind path for some common directories
BINDPATH=""
for i in /sbb /home ${JOBDIR} ${TASKDIR} ; do
  dir=`readlink -f $i`
  if [ -d ${dir} ] ; then
    BINDPATH="${dir},${BINDPATH}"
  fi
done
export SINGULARITY_BINDPATH=${BINDPATH}

# Store random seed
SEED=`od -vAn -N4 -tu4 < /dev/urandom | tr -d ' '`
echo "Seed is ${SEED}"
echo ${SEED} > seed

# Store arguments
echo "$@" > args

echo "Starting job ${JOB} task ${TASK} at: `date`"

# Setup
singularity run -B /sbb:/sbb ${SIF} create_kasper_user_directory.sh `pwd`

# Copy configuration
XMLDIR=$HOME/git/bl3-kassiopeia
XML=ProtonTrapSimulation.xml
cp -r ${XMLDIR}/*.xml .
for dir in electrostatic electromagnet field geometry ; do
  cp -r ${XMLDIR}/${dir} .
done
for dir in maps ; do
  ln -sf ${XMLDIR}/${dir} .
done

# Copy cached field
cp -n -r ~/.cache/KEMField/* cache/KEMField/

# Simulation
singularity run ${SIF} Kassiopeia ${XML} -r run=${TASK} seed=${SEED} graphics=0 writesteps=0 "$@"

# Cleanup
if [ ${TASK} -gt 1 ] ; then rm -r cache/KEMField/ ; fi
if [ ${TASK} -gt 0 ] ; then rm -r data config ; fi
# Compress
gzip log/Kassiopeia/*

# Rename output
pushd output/Kassiopeia
mv ProtonTrapSimulation_${TASK}.root ProtonTrapSimulation_${JOB}_${TASK}.root
popd

# Combine
if [ ${SLURM_ARRAY_TASK_ID} -eq 1 ] ; then
cat << EOF > $HOME/scratch/ProtonTrapSimulation_${SLURM_ARRAY_JOB_ID}.sh
#!/bin/bash
#SBATCH --job-name=ProtonTrapCombine_${SLURM_ARRAY_JOB_ID}
#SBATCH --dependency=afterok:${SLURM_ARRAY_JOB_ID}
#SBATCH --mem-per-cpu=2gb
#SBATCH --time=01:00:00
#SBATCH --output=%A.out
#SBATCH --account=${SLURM_JOB_ACCOUNT}
module load gcc/5.2 root-cern/6.14.04
hadd $HOME/scratch/ProtonTrapSimulation_${SLURM_ARRAY_JOB_ID}.root \
     $HOME/scratch/${SLURM_ARRAY_JOB_ID}/*/output/Kassiopeia/ProtonTrapSimulation_${SLURM_ARRAY_JOB_ID}_*.root
seff-array ${SLURM_ARRAY_JOB_ID} > $HOME/scratch/ProtonTrapSimulation_${SLURM_ARRAY_JOB_ID}.seff
tar -czvf $HOME/scratch/ProtonTrapSimulation_${SLURM_ARRAY_JOB_ID}.tar.gz ${SUBMITDIR}/${SLURM_ARRAY_JOB_ID}_*.out
EOF
chmod +x $HOME/scratch/ProtonTrapSimulation_${SLURM_ARRAY_JOB_ID}.sh
sbatch $HOME/scratch/ProtonTrapSimulation_${SLURM_ARRAY_JOB_ID}.sh
fi

echo "Finished job ${JOB} task ${TASK} at: `date`"
