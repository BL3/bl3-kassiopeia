#!/bin/bash

dl="https://docs.google.com/uc?export=download&id"
dir="maps/Cryogenic"
mkdir -p $dir
wget -nc -O $dir/Z0-80med_output.vti    $dl=1Rd9ZE8ANap23WcrXQn9T9YVJSohegL_T
wget -nc -O $dir/Z80-100med_output.vti  $dl=1XMHlD4QFTxQvrJi1N5OZ7almJUZ4U78z
wget -nc -O $dir/Z100-145med_output.vti $dl=1712NrJgNNOalet6EXiuD0uFxUdCakQvx
wget -nc -O $dir/Z145-155med_output.vti $dl=1AcAc7_Q6DWqKEhFIYoD1r7mWHlKqCe-L
wget -nc -O $dir/Z0-80med_output_100pct.vti    $dl=1uozgfxLTuruZhZ3T62aJcodTXLkjMCZm
wget -nc -O $dir/Z80-100med_output_100pct.vti  $dl=1EsfAhS0fvxKlkZG8NX28pTaLfneXaniP
wget -nc -O $dir/Z100-145med_output_100pct.vti $dl=1cldM-QK0ZydW0XXqGEkgBSlP2_nArH5_
wget -nc -O $dir/Z145-155med_output_100pct.vti $dl=1QibHpsZKsCCq7sTA4Cx7XQ1RT7lQp86H
