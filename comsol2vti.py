#!/usr/bin/env python

import vtk
import argparse
import numpy as np
import pandas as pd
from tqdm.notebook import trange

def main():

    parser = argparse.ArgumentParser(
        prog='comsol2vti',
        description='COMSOL to VTK conversion',
        usage='%(prog)s [options] filename')

    parser.add_argument('-f', '--field', action='store', choices=['V', 'B'], help='field to extract', default='B')
    parser.add_argument('-i', '--infile', help='input filename')
    parser.add_argument('-o', '--outfile', help='output filename')
    args = parser.parse_args()
    in_name = args.infile
    out_name = args.outfile
    field = args.field
    cm = 0.01

    # Read map into memory
    df = pd.read_table(
        in_name,
        sep='\s+',
        comment='%',
        names=['x','y','z','Ex','Ey','Ez','Bx','By','Bz'],
        usecols=['x','y','z','Bx','By','Bz'],
        dtype=np.float64)

    # Describe data
    print(df.describe())

    # Calculate upper and lower bounds and dimensionality
    origin = [None]*3
    spacing = [None]*3
    bounds = [None]*6
    dimensions = [None]*3
    entry = 1
    for i in range(3):
        origin[i] = df.min()[i]*cm
        bounds[2*i] = df.min()[i]*cm
        bounds[2*i+1] = df.max()[i]*cm
        spacing[i] = round(df.iloc[entry,i] - df.iloc[entry-1,i],5)*cm
        dimensions[i] = int(round((bounds[2*i+1] - bounds[2*i]) / (spacing[i]) + 1))
        entry *= dimensions[i]

    # Verify calculated length
    if (len(df) != np.prod(dimensions)):
        print('Expected grid for ' + len(df) + ', but dimensions are ' + dimensions)
        exit()

    # Create fields
    grid = vtk.vtkImageData()
    grid.SetDimensions(dimensions)
    grid.SetSpacing(spacing)
    grid.SetOrigin(origin)
    numPoints = grid.GetNumberOfPoints()

    validityData = vtk.vtkIntArray()
    validityData.SetName("validity")
    validityData.SetNumberOfComponents(1)
    validityData.SetNumberOfTuples(numPoints)
    grid.GetPointData().AddArray(validityData);

    if (field == 'V'):
        fieldData = vtk.vtkDoubleArray()
        fieldData.SetName("electric potential")
        fieldData.SetNumberOfComponents(1)
        fieldData.SetNumberOfTuples(numPoints)
        grid.GetPointData().AddArray(fieldData)

    if (field == 'B'):
        fieldData = vtk.vtkDoubleArray()
        fieldData.SetName("magnetic field")
        fieldData.SetNumberOfComponents(3)
        fieldData.SetNumberOfTuples(numPoints)
        grid.GetPointData().AddArray(fieldData)

    # Copy point values
    for i in trange(numPoints):
        gridpoint = np.array(grid.GetPoint(i))
        fieldpoint = np.array([df['x'][i], df['y'][i], df['z'][i]])*cm
        if np.linalg.norm(fieldpoint - gridpoint, ord = np.inf) > 0.1*spacing[0]:
            print(i, gridpoint, fieldpoint, np.linalg.norm(fieldpoint - gridpoint))

        if (field == 'V'):
            fieldData.SetTuple1(i, df['V'][i])
            validityData.SetTuple1(i, 1)

        if (field == 'B'):
            fieldData.SetTuple3(i, df['Bx'][i], df['By'][i], df['Bz'][i])
            validityData.SetTuple1(i, 2)

    # Write outConnection
    xmlImageWriter = vtk.vtkXMLImageDataWriter()
    xmlImageWriter.SetFileName(out_name)
    xmlImageWriter.SetInputData(grid)
    xmlImageWriter.Write()

if __name__ == '__main__':
    main()
