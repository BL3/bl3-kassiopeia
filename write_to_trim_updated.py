import matplotlib.pyplot as plt
import uproot
import numpy as np
import math
from math import sin, cos, atan, tan, pi, acos
tree=uproot.open("ProtonTrapSimulation_4377335.root")
track=tree.get("component_track_world_DATA")
initial_kinetic_energy, creator_name, initial_position_x, initial_position_y,final_time,final_momentum_x, final_momentum_y, final_momentum_z, x, y, z, initial_position_z, terminator_name, final_theta, final_phi, final_kinetic_energy= track.arrays(["initial_kinetic_energy", "creator_name","initial_position_x", "initial_position_y","final_time","final_momentum_x", "final_momentum_y" ,"final_momentum_z","final_position_x","final_position_y", "final_position_z","initial_position_z","terminator_name", "final_polar_angle_to_z", "final_polar_angle_to_b","final_kinetic_energy"], outputtype = tuple)
print(len(creator_name))
cut=[]
cosx=[]
cosy=[]
cosz=[]
n = [0, sin(pi/10), cos(pi/10)]
x = [1, 0, 0]
y = [0, cos(pi/10), -sin(pi/10)]
for i in range(len(terminator_name)):
    if terminator_name[i]==b'term_proton_target' and initial_position_z[i]>0.35 and initial_position_z[i]<0.65:
        cut.append(i)
    final_momentum = np.sqrt(final_momentum_x[i]**2 + final_momentum_y[i]**2 + final_momentum_z[i]**2)
    cosx.append((final_momentum_x[i]*n[0] + final_momentum_y[i]*n[1] + final_momentum_z[i]*n[2])/final_momentum)
    cosy.append((final_momentum_x[i]*x[0] + final_momentum_y[i]*x[1] + final_momentum_z[i]*x[2])/final_momentum)
    cosz.append((final_momentum_x[i]*y[0] + final_momentum_y[i]*y[1] + final_momentum_z[i]*y[2])/final_momentum)
cosx=np.array(cosx)
cosy=np.array(cosy)
cosz=np.array(cosz)
with open('TRIM-4377335.txt', "w") as text_file:
    for i in cut:
        text_file.write("%-7i%-6g%-9.1f%-9.1f%-8.1f%-9.1f%-9.3f%-9.3f%.3f\r\n"%(i,1,final_kinetic_energy[i],0,0,0,cosx[i],cosy[i],cosz[i]))